﻿using System;
using System.Collections.Generic;
using System.Text;

namespace utb_eshop_patek.Domain.Repositories
{
    public interface IRepository<TEntity>
    {
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Remove(TEntity entity);
        TEntity Get(Func<TEntity, bool> predicate);
        IList<TEntity> GetAll();
        IList<TEntity> GetAll(Func<TEntity, bool> predicate);
    }
}
