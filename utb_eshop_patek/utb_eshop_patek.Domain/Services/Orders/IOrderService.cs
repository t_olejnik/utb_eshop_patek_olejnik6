﻿using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Orders;

namespace utb_eshop_patek.Domain.Services.Orders
{
    public interface IOrderService
    {
        void CreateOrder(int userID, string userTrackingCode);
        IList<Order> GetOrders(int userID);
    }
}
