﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Domain.Entities.Addresses;

namespace utb_eshop_patek.Infrastructure.Data.Configurations.Addresses
{
    public class AddressConfiguration : IEntityTypeConfiguration<Address>
    {
        public void Configure(EntityTypeBuilder<Address> builder)
        {
            builder.ToTable("Addresses", "Web");

            builder.HasKey(e => e.ID);
        }
    }
}
