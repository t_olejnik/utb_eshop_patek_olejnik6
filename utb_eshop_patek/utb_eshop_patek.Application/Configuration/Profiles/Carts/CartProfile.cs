﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using utb_eshop_patek.Application.Client.ViewModels.Carts;
using utb_eshop_patek.Domain.Entities.Carts;

namespace utb_eshop_patek.Application.Configuration.Profiles.Carts
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMaps();
        }

        private void CreateMaps()
        {
            CreateMap<CartItem, CartItemViewModel>()
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.Product.ImageURL))
                .ForMember(dest => dest.ProductName, opt => opt.MapFrom(src => src.Product.Name))
                .ForMember(dest => dest.Price, opt => opt.MapFrom(src => src.Product.Price))
                .ForMember(dest => dest.ProductID, opt => opt.MapFrom(src => src.Product.ID));
        }
    }
}
